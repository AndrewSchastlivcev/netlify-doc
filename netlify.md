Админка Netlify [Login](https://app.netlify.com/) `администрирование системы деплоя сайта`

------

Авторизоваться с помощью email 

![](C:\Users\Admin\OneDrive\Desktop\manual\images\login.png)

![](C:\Users\Admin\OneDrive\Desktop\manual\images\netlify_controls.png)

выбрать проэкт sparkybit

![](C:\Users\Admin\OneDrive\Desktop\manual\images\identy.png)

> `Identity` хранит информацию  о юзерах и  `Git Gateway`
>
> `Git Gateway` подключаеться к `GitHub API`  и позволяет юзерам из `identity` редактировать контент на `GitHub` без использования аккаунта `GitHub`
>
> Эту цепочку использует `Netlify CMS` 

1. Общая информация о проэкте

2. Лог и управление деплоем ( минимальный набор как в TeamCity )

3. Управления авторизацией

4. Все настройки проэкта

5. Быстрый переход к настройкам сервиса Identity 

6. Отправить приглашение на почтовый ящик новому юзеру

7. При клике на кадого юзера, осуществляеться переход к его оциям

   ![](C:\Users\Admin\OneDrive\Desktop\manual\images\user.png)

- Сброс пароля 

- Редактирование информации юзера

- удаление профиля юзера

  